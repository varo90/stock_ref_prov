<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Referencias Artículos</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jquery -->
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <link href="css/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="css/jquery-ui.theme.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/reservas.css" rel="stylesheet">
    <!-- Sign in bootstrap -->
    <link href="css/signin.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="css/dataTables.responsive.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="css/jquery.dataTables_themeroller.css" rel="stylesheet">
    <!-- jQuery -->
    <script type="text/javascript" charset="utf8" src="jquery/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="js/jquery-ui.min.js"></script>
    <!-- DataTables -->
    <script type="text/javascript" src="jquery/dataTables.responsive.js"></script>
    <script type="text/javascript" src="jquery/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="jquery/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="jquery/dataTables.buttons.min.js"></script>
    <!-- Bootbox -->
    <script type="text/javascript" src="jquery/bootbox.min.js"></script>
    <!-- Customized js -->
    <script type="text/javascript" charset="utf8" src="js/aeat.js"></script>
    <!-- Elegant TextArea -->
    <!--<script src="vendor/bootstrap/js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>-->

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="../../link/index.php"><img src='images/logo-se.png' alt='SE Logo'></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <?php
            if(basename($_SERVER['SCRIPT_FILENAME']) == 'login.php') { ?>
                <li class="nav-item">
                  <a class="nav-link" href="login.php">Acceder</a>
                </li>
            <?php
            }
            else { ?>
                <li class="nav-item">
                  <a class="nav-link" href=""><?php echo $_SESSION['username_link']; ?></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="logout.php">Salir</a>
                </li>
            <?php
            }
            ?>
          </ul>
        </div>
    </nav>
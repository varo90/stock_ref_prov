<?php

class queries {
    
    static function find_reference($conditions) {
     
     $sql = "SELECT DISTINCT oi.U_GSP_REFERENCE AS 'reference', mc.U_GSP_CATALOGDESC AS 'catalog', oi.U_GSP_Season AS 'season', CAST(SUM(oiw.OnHand)  as int)
        FROM SBO_EULALIA.dbo.[@GSP_TCMODELCATALOG] AS mc
            LEFT JOIN SBO_EULALIA.dbo.OITM as oi ON oi.U_GSP_MODELCODE = mc.U_GSP_MODELCODE
            LEFT JOIN SBO_EULALIA.dbo.OITW as oiw ON oi.ItemCode = oiw.ItemCode
        WHERE $conditions
        GROUP BY oi.U_GSP_REFERENCE, mc.U_GSP_CATALOGDESC,oi.U_GSP_Season
        HAVING SUM(oiw.OnHand) > 0";

     /*   $sql = " SELECT DISTINCT oi.U_GSP_REFERENCE AS 'reference', mc.U_GSP_CATALOGDESC AS 'catalog',oi.U_GSP_Season AS 'season'
        FROM SBO_EULALIA.dbo.[@GSP_TCMODELCATALOG] AS mc
            LEFT JOIN SBO_EULALIA.dbo.OITM as oi ON oi.U_GSP_MODELCODE = mc.U_GSP_MODELCODE
        WHERE $conditions";*/
        return $sql;
    }
    
    function join_conditions($filters,$sql_ref,$sql_col) {
        $texts = array();
        foreach($filters as $filter) {
            if($filter == null) {continue;}
            $texts[] = "($sql_ref='$filter->reference' AND $sql_col='$filter->color')";
        }
        $conditions = implode(' OR ', $texts);
        return $conditions;
    }
}

<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('db_connections.php');
    include('queries.php');
    include('csv_files.php');

    
    $fichero_subido = $_GET['fich'];
    $prov_refs = file('uploaded/'.$fichero_subido, FILE_IGNORE_NEW_LINES);
    
    $refs_prov = array();
    foreach($prov_refs as $cont => $row) {
        $pr = str_getcsv($row, ";"); //parse the items in rows 
        $catalog = explode(' ',$pr[0]);
        if($catalog[0] != '') {
            $refs_prov[] = "mc.U_GSP_CATALOGDESC LIKE '"."%" . $catalog[0] . "%'";
        }
    }
    $conditions = implode(' OR ', $refs_prov);

    $db_ms = new db();
    $items = $db_ms->make_query(queries::find_reference($conditions),[],PDO::FETCH_ASSOC);
    unset($db_ms);

    $csv = new csv_files('downloaded/export_items_prov.csv');
    foreach($items as $cont => $row) {
        $csv->write($row);
    }
    unset($csv);

    header('location: downloaded/export_items_prov.csv');
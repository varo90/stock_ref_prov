<?php

    $nom_fich = basename($_FILES['uploadedfile']['name']);
    $extension = explode('.', $nom_fich)[1];
    $destination = 'uploaded/'.$nom_fich;

    if($extension == 'csv') {
        echo $destination;
        echo '<br>';
        if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $destination)) {
            header("location:compare_refs.php?fich=$nom_fich");
        } else {
            echo "Fichero no se pudo cargar.";
        }
    } else {
        echo "Fichero no cargado.<br>Debe ser un fichero .csv.<br>";
        echo 'Extensión: ' . $extension . '<br>';
        echo 'Nombre: ' . $nom_fich;
    }
    
?>
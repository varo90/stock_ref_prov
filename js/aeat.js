var calendar;
var country = "<?php echo $_SESSION['country']; ?>";
function renderCalendar() {
    calendar = $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      lang: country,
      selectable: true,
      selectHelper:true,
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      displayEventEnd: true,
      snapDuration: '00:05:00',
      loading: function(bool) {
        $('#loading').toggle(bool);
      },
      eventOverlap:false,
      select: function(start, end, allDay) {
        var title = "Nueva Reserva"; 
          eventData = {
            title: title,
            start: start,
            end: end
          };
          user_id = "<?php echo $_SESSION['userid']; ?>";
          $('#calendar').fullCalendar('renderEvent', eventData, true);
           start = moment(start).format("YYYY-MM-DD HH:mm:ss");
           end = moment(end).format("YYYY-MM-DD HH:mm:ss"); // stick? = true          );
           var reason = prompt("Descripción de la Reserva");
            if(reason===null){
              $('#calendar').fullCalendar('removeEvents');
              $('#calendar').fullCalendar('refetchEvents');
            }else{
              $.ajax({
                      url: 'scripts/add_event.php',
                      data: 'start='+ start +'&end='+ end +'&user_id='+user_id + '&store_id='+ current_store+'&reason='+reason,
                      type: "GET",
                      success: function(json) {
                            $('#calendar').fullCalendar('removeEvents');
                            $('#calendar').fullCalendar('refetchEvents');
                     }
              });
           }

      $('#calendar').fullCalendar('unselect');
      },
      eventDrop: function(event, delta) {
                       var start = moment(event.start).format("YYYY-MM-DD HH:mm:ss");
                       var end = moment(event.end).format("YYYY-MM-DD HH:mm:ss");
                       if (end <= start) {
                          start = moment(start).add(10, 'hours').format("YYYY-MM-DD HH:mm:ss");
                          end = moment(start).add(8, 'hours').format("YYYY-MM-DD HH:mm:ss");
                       }
                       if (end=="Invalid date") end= start;
                       $.ajax({
                           url: 'scripts/update_events.php',
                           data: 'start='+ start +'&end='+ end +'&event_id='+ event.event_id ,
                           type: "GET",
                           success: function(json) {
                            if (moment(start).hours() == 10 && moment(end).hours() == 18) {
                                  $('#calendar').fullCalendar('removeEvents');
                                  $('#calendar').fullCalendar('refetchEvents');
                            }
                           }
                       });
                   },
      eventResize: function(event) {
                         var start = moment(event.start).format("YYYY-MM-DD HH:mm:ss");
                         var end = moment(event.end).format("YYYY-MM-DD HH:mm:ss");
                         if (end=="Invalid date") end= start;
                         $.ajax({
                             url: 'scripts/update_events.php',
                             data: 'start='+ start +'&end='+ end +'&event_id='+ event.event_id ,
                             type: "GET",
                             success: function(json) {
                             }
                         });
                     },
      eventClick: function(event){
        var event_user_id = event['user_id'];
        var user_id = "<?php echo $_SESSION['userid']; ?>";

        if (user_id == event_user_id || user_id == 14 || user_id == 125) {

            if (confirm('¿ Estas seguro de borrar esta reserva ?')) {
              $.ajax({
                      url: 'scripts/delete_event.php',
                      data: 'event_id=' + event.event_id,
                      type: "GET",
                      success: function() {
                        $(function() {
                              $.bootstrapGrowl("Se ha borrado con exito", {
                                  type: 'success',
                                  align: 'center',
                                  width: 'auto',
                                  allow_dismiss: false
                              });
                        });
                          $('#calendar').fullCalendar('removeEvents');
                          $('#calendar').fullCalendar('refetchEvents');

                     }
                });
            }
        }
      },
      eventRender: function(event, element) {
        var event_user_id = event['user_id'];
        var user_id = "<?php echo $_SESSION['userid']; ?>";
        if (user_id == event_user_id || user_id == 14) {
        } else {
          event.editable = false;
        }
        $(element).tooltip({title: event.title});  
      }
    });
  }


function load_stores() {
    $.get("scripts/storelist.php", {},function(data,status){
            $('#StoreList').html(data);
            var selected = document.newticket.store_select.value;
            current_store = selected;



            renderCalendar();
            $('#calendar').fullCalendar('addEventSource',"scripts/events.php?store_id="+current_store);
            $('#calendar').fullCalendar('refetchEvents');
             if (current_store == 0) {
                  $("#calendar").hide();

                } else {
                  $("#calendar").show();

                }

            $('#store_select').on('change', function (e) {
                var selected = document.newticket.store_select.value;
                var old_store = current_store;
                current_store = selected;

                if (current_store == 0) {
                  $("#calendar").hide();

                } else {
                  $("#calendar").show();

                }

                calendar.fullCalendar('removeEvents');
                calendar.fullCalendar('removeEventSource',"scripts/events.php?store_id="+old_store);
                calendar.fullCalendar('addEventSource',"scripts/events.php?store_id="+current_store);
                calendar.fullCalendar('refetchEvents');
            });
    });
}
function scroll_aside() {
  var change = false;
  $(window).scroll(function(){
      window_y = $(window).scrollTop();
      under_header = parseInt($("#headerInfo").height());
      if (window_y > under_header) {
         $("#listCol").css({"position": "fixed", "top": "0%"});
      } else {
         $("#listCol").css({"position": "relative", "top": ""});
      }
  });
}
function test() {
   $.get("../scripts/check_avail.php", {StoreCode:'a', from_day:'a', to_day:'a', amount:'a'},function(data,status){
        bootbox.dialog({
                        message: data,
                        title: "<strong><center>Consulta de disponibilidad</center></strong>",
                        buttons: {
                          success: {
                            label: "Cerrar",
                            className: "btn-primary"
                          }
                        }
                  });  
});
        }

$(document).ready(function() {

  load_stores();

  //scroll_aside();
});